#!/usr/bin/env ruby
#
require 'bit_vector'

a = BitVector.new(13) # 13 bits long, all zero
puts "Using bit vector: #{a.to_s}"
puts "The first set bit is #{a.index(1,0)}"
a[3] = 1
a[4] = 2
puts "Using bit vector: #{a.to_s}"
puts "The higher bits are all #{a[1930]} (zero)"

b = BitVector.new("00001011011")
puts "Using bit vector: #{b.to_s}"
puts "The first bit is #{b[0]}"
puts "The last bit is #{b[10]}"
puts "The first set bit is #{b.index(1,0)}"
puts "The first clear bit after the 5th is #{b.index(0,5)}"

c = BitVector.new("11111111011")
puts "Using bit vector: #{c.to_s}"
puts "The first clear bit is #{c.index(0,1)}"
