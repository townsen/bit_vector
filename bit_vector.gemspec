$:.push File.expand_path("../lib", __FILE__)

require 'bit_vector/version'

BitVector::GemSpec = Gem::Specification.new do |s|
  s.name        = %q{bit_vector}
  s.version     = BitVector::VERSION
  s.required_ruby_version = ">=2.0.0"
  s.date        = Time.now.strftime("%Y-%m-%d")
  s.authors     = ['Nick Townsend']
  s.email       = ['nick.townsend@mac.com']
  s.summary     = %q{Ruby BitVector Implementation}
  s.homepage    = %q{https://github.com/townsen/bit_vector/}
  s.has_rdoc    = false
  s.files       = Dir['lib/**/*.rb'] + Dir['test/**/*.rb'] + Dir['ext/**/*']
  s.files       += %w{ Rakefile README.md .gemtest }
  s.description = "BitVector for Ruby implemented in C"
  s.extensions  = ["ext/bit_vector/extconf.rb"]
  s.license     = 'MIT'
  s.add_development_dependency 'minitest', '~> 4.3', '>= 4.3.2'
end
