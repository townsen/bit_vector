require "mkmf"

cflags = %w{ -std=c99 }
cflags += %w{ -Wall -Wextra -O0 -g } if ENV['DEBUG']

with_cflags(cflags.join(' ')) do
  create_makefile("bit_vector") do |conf|
    if RUBY_PLATFORM =~ /darwin/
      conf.map{|l| l.gsub(/^ARCH_FLAG.*/, 'ARCH_FLAG = -arch x86_64') }
    else
      conf
    end
  end
end
