# coding: ascii-8bit
#
require 'test/unit'
require 'bit_vector'

class TC_BitVector < Test::Unit::TestCase

  # BitVector.new
  def test_s_new_nil
    a = BitVector.new
    assert_equal(1, a.size)
    assert_equal("0", a.to_s)
  end

  def test_s_new_fixnum
    assert_raises(ArgumentError) {
      a = BitVector.new(0)
    }

    assert_raises(ArgumentError) {
      a = BitVector.new(-1)
    }

    [1,7,8,15,16,255,256,65535,65536].each {|n|
      a = BitVector.new(n)
      assert_equal(n, a.size)
      assert_equal("0" * n, a.to_s)
    }

  end

  def test_s_new_string
    a = BitVector.new("00000")
    assert_equal(5, a.size)
    assert_equal("00000", a.to_s)

    a = BitVector.new("0000011111")
    assert_equal(10, a.size)
    assert_equal("0000011111", a.to_s)
  end

  def test_s_new_other
    assert_raises(ArgumentError) {
      a = BitVector.new({})
    }

    assert_raises(ArgumentError) {
      a = BitVector.new(:symbol)
    }

    assert_raises(ArgumentError) {
      a = BitVector.new(1.0)
    }
  end

  def test_s_new_GC
    ary = Array.new(10)
    assert_nothing_raised {
      200.times do
        idx  = rand(10)
        size = rand(1000000) + 1
        bits = BitVector.new(size)
        ary[idx] = bits
      end
    }
  end

  # BitVector.from_bytes
  def test_s_from_bytes
    a = BitVector.from_bytes("a")
    assert_equal(8, a.size)
    assert_equal("10000110", a.to_s)

    a = BitVector.from_bytes("ab")
    assert_equal(16, a.size)
    assert_equal("1000011001000110", a.to_s)

    # a\0b
    a = BitVector.from_bytes("a\0b")
    assert_equal(24, a.size)
    assert_equal("100001100000000001000110", a.to_s)
  end

  # BitVector#to_bytes
  def test_to_bytes_byte
    a = BitVector.new("10000000")
    assert_equal("\x01", a.to_bytes)
  end

  def test_to_bytes_qw
    a = BitVector.new("1000000000000000000000000000000000000000000000000000000000000000")
    assert_equal("\x01\x00\x00\x00\x00\x00\x00\x00", a.to_bytes)
  end

  def test_to_bytes_nul
    a = BitVector.new(24)
    assert_equal("\x00\x00\x00", a.to_bytes)
  end

  # BitVector#to_s
  def test_to_s
    (0..255).each {|i|
      a = BitVector.from_bytes(i.chr)
      bit = ("%08b" % i).reverse
      assert_equal(bit, a.to_s)
    }
  end

  # BitVector#size
  def test_size_getter
    [1,7,8,15,16,255,256,65535,65536,4294967295,4294967296,4294967297].each {|n|
      a = BitVector.new(n)
      assert_equal(a.size, n)
    }
  end

  # BitVector#size=
  def test_size_setter
    a = BitVector.new()
    [1,7,8,15,16,255,256,65535,65536,4294967295,4294967296,4294967297].each {|n|
      a.size = n
      assert_equal(n, a.size)
    }

    assert_raises(ArgumentError) {
      a.size = 0
    }

    assert_raises(ArgumentError) {
      a.size = -1
    }

  end

  # BitVector#length
  def test_length
    [1,7,8,15,16,255,256,65535,65536,4294967295,4294967296,4294967297].each {|n|
      a = BitVector.new(n)
      assert_equal(a.length, n)
    }
  end

  # BitVector#dup
  def test_dup
    a = BitVector.new("0")
    b = a.dup
    assert_equal("0", b.to_s)

    a = BitVector.new("10101010101010101")
    b = a.dup
    assert_equal("10101010101010101", b.to_s)
  end

  # BitVector#index
  def test_index
    a = BitVector.new("00101010101010101")
    assert_equal(2, a.index(1,0))
    assert_equal(2, a.index(1,2))
    assert_equal(4, a.index(1,3))
  end

  # BitVector#slice
  def test_slice
    a = BitVector.new("10101010101010101")
    b = a.slice(2..7)
    assert_equal("101010", b.to_s)
  end

  # BitVector#clone
  def test_clone
    a = BitVector.new("0")
    b = a.clone
    assert_equal("0", b.to_s)

    a = BitVector.new("10101010101010101")
    b = a.clone
    assert_equal("10101010101010101", b.to_s)
  end

  # BitVector#get
  def test_get
    a = BitVector.new("1")
    assert_equal(1, a.get(0))
    assert_equal(0, a.get(1))
    assert_equal(0, a.get(100))

    a = BitVector.new("10"*50)
    assert_equal(1, a.get(98))
    assert_equal(0, a.get(99))
  end

  # BitVector#set
  def test_set
    a = BitVector.new
    a.set 1,1
    assert_equal("01", a.to_s)
    a.set 5,1
    assert_equal("010001", a.to_s)
    a.set 6,1
    assert_equal("0100011", a.to_s)
    a.set 5,0
    assert_equal("0100001", a.to_s)
  end

  # BitVector#on
  def test_on
    a = BitVector.new
    a.on 3
    assert_equal("0001", a.to_s)
    a.on 9
    assert_equal("0001000001", a.to_s)
  end

  # BitVector#on range
  def test_on_range
    a = BitVector.new
    a.on 1..3
    assert_equal("0111", a.to_s)
    a.on 6..10
    assert_equal("01110011111", a.to_s)
  end

  # BitVector#off
  def test_off
    a = BitVector.new("11111111111111111111")
    a.off 5
    assert_equal("11111011111111111111", a.to_s)
    a.off 9
    assert_equal("11111011101111111111", a.to_s)
  end

  # BitVector#off
  def test_off_range
    a = BitVector.new("11111111111111111111")
    a.off 2..4
    assert_equal("11000111111111111111", a.to_s)
    a.off 6..10
    assert_equal("11000100000111111111", a.to_s)
  end

  # BitVector#clear
  def test_clear
    a = BitVector.new("0")
    a.clear
    assert_equal(1, a.size)
    assert_equal("0", a.to_s)

    a = BitVector.new("1111")
    a.clear
    assert_equal(4, a.size)
    assert_equal("0000", a.to_s)

    a = BitVector.new("111100001111000011")
    a.clear
    assert_equal(18, a.size)
    assert_equal("000000000000000000", a.to_s)
  end

  # BitVector#|
  def test_or
    a = BitVector.new("000000")
    b = BitVector.new("111000")
    x = a | b
    assert_equal("000000", a.to_s)
    assert_equal("111000", b.to_s)
    assert_equal("111000", x.to_s)

    a = BitVector.new("0101010101")
    b = BitVector.new("1111100000")
    x = a | b
    assert_equal("0101010101", a.to_s)
    assert_equal("1111100000", b.to_s)
    assert_equal("1111110101", x.to_s)

    a = BitVector.new("00000000000001")
    b = BitVector.new("00000000000010")
    x = a | b
    assert_equal("00000000000011", x.to_s)

    a = BitVector.new("1")
    b = BitVector.new("000000001")
    x = a | b
    assert_equal("100000001", x.to_s)

    a = BitVector.new("000000001")
    b = BitVector.new("1")
    x = a | b
    assert_equal("100000001", x.to_s)
  end

  # BitVector#+
  def test_plus
    a = BitVector.new("000000")
    b = BitVector.new("111000")
    x = a + b
    assert_equal("000000", a.to_s)
    assert_equal("111000", b.to_s)
    assert_equal("111000", x.to_s)

    a = BitVector.new("0101010101")
    b = BitVector.new("1111100000")
    x = a + b
    assert_equal("0101010101", a.to_s)
    assert_equal("1111100000", b.to_s)
    assert_equal("1111110101", x.to_s)

    a = BitVector.new("00000000000001")
    b = BitVector.new("00000000000010")
    x = a + b
    assert_equal("00000000000011", x.to_s)

    a = BitVector.new("1")
    b = BitVector.new("000000001")
    x = a + b
    assert_equal("100000001", x.to_s)

    a = BitVector.new("000000001")
    b = BitVector.new("1")
    x = a + b
    assert_equal("100000001", x.to_s)
  end

  # BitVector#-
  def test_minus
    a = BitVector.new("01010101")
    b = BitVector.new("00001111")
    x = a - b
    assert_equal("01010101", a.to_s)
    assert_equal("00001111", b.to_s)
    assert_equal("01010000", x.to_s)

    a = BitVector.new("111111111111111111111111")
    assert_equal(24, a.size)
    b = BitVector.new("111111111")
    assert_equal(9, b.size)
    x = b - a
    assert_equal("000000000", x.to_s)
    x = a - b
    assert_equal("000000000111111111111111", x.to_s)
  end

  # BitVector#<=>
  def test_cmp
    a = BitVector.new("0")
    b = BitVector.new("1")
    assert_equal(-1, a <=> b)
    assert_equal(0, a <=> a)
    assert_equal(1, b <=> a)

    a = BitVector.new("1000000000")
    b = BitVector.new("1")
    assert_equal(0, a <=> b)
  end

  # BitVector#&
  def test_and
    a = BitVector.new("010111")
    b = BitVector.new("001110")
    x = a & b
    assert_equal("010111", a.to_s)
    assert_equal("001110", b.to_s)
    assert_equal("000110", x.to_s)

    a = BitVector.new("01010101")
    b = BitVector.new("00001111")
    x = a & b
    assert_equal("01010101", a.to_s)
    assert_equal("00001111", b.to_s)
    assert_equal("00000101", x.to_s)

    a = BitVector.new("0")
    b = BitVector.new("1111111111")
    x = a & b
    assert_equal("0", x.to_s)
    x = b & a
    assert_equal("0000000000", x.to_s)

    a = BitVector.new("11111111")
    b = BitVector.new("1111111111111111")
    x = a & b
    assert_equal("11111111", x.to_s)
    x = b & a
    assert_equal("1111111100000000", x.to_s)

    a = BitVector.new("11001100")
    b = BitVector.new("1110111011101110")
    x = a & b
    assert_equal("11001100", x.to_s)
    x = b & a
    assert_equal("1100110000000000", x.to_s)
  end

  # BitVector#^
  def test_xor
    a = BitVector.new("01010101")
    b = BitVector.new("00110011")
    x = a ^ b
    assert_equal("01010101", a.to_s)
    assert_equal("00110011", b.to_s)
    assert_equal("01100110", x.to_s)

    a = BitVector.new("0")
    b = BitVector.new("10101010101010")
    x = a ^ b
    assert_equal("10101010101010", x.to_s)
    x = b ^ a
    assert_equal("10101010101010", x.to_s)
  end

  # BitVector#*
  def test_multi
    a = BitVector.new("010111")
    b = BitVector.new("001110")
    x = a * b
    assert_equal("010111", a.to_s)
    assert_equal("001110", b.to_s)
    assert_equal("000110", x.to_s)

    a = BitVector.new("01010101")
    b = BitVector.new("00001111")
    x = a * b
    assert_equal("01010101", a.to_s)
    assert_equal("00001111", b.to_s)
    assert_equal("00000101", x.to_s)

    a = BitVector.new("0")
    b = BitVector.new("1111111111")
    x = a * b
    assert_equal("0", x.to_s)
    x = b * a
    assert_equal("0000000000", x.to_s)

    a = BitVector.new("11111111")
    b = BitVector.new("1111111111111111")
    x = a * b
    assert_equal("11111111", x.to_s)
    x = b * a
    assert_equal("1111111100000000", x.to_s)

    a = BitVector.new("11001100")
    b = BitVector.new("1110111011101110")
    x = a * b
    assert_equal("11001100", x.to_s)
    x = b * a
    assert_equal("1100110000000000", x.to_s)
  end

  # BitVector#~
  def test_not
    a = BitVector.new("0")
    x = ~a
    assert_equal("0", a.to_s)
    assert_equal("1", x.to_s)

    a = BitVector.new("0101")
    x = ~a
    assert_equal("1010", x.to_s)

    a = BitVector.new("00000000001111111111")
    x = ~a
    assert_equal("11111111110000000000", x.to_s)
  end

  # BitVector#zero?
  def test_zero?
    a = BitVector.new("0")
    assert_equal(true, a.zero?)
    a = BitVector.new("0"*100)
    assert_equal(true, a.zero?)

    a = BitVector.new("1")
    assert_equal(false, a.zero?)
    a = BitVector.new("0"*100 + "1")
    assert_equal(false, a.zero?)

  end

  # BitVector#nonzero?
  def test_nonzero?
    a = BitVector.new("0")
    assert_equal(false, a.nonzero?)
    a = BitVector.new("0"*100)
    assert_equal(false, a.nonzero?)

    a = BitVector.new("1")
    assert_equal(true, a.nonzero?)
    a = BitVector.new("0"*100 + "1")
    assert_equal(true, a.nonzero?)
  end

  # BitVector#max
  def test_max
    a = BitVector.new("0")
    assert_equal(nil, a.max)
    a = BitVector.new("1")
    assert_equal(0, a.max)
    a = BitVector.new("11")
    assert_equal(1, a.max)
    a = BitVector.new("10000000001")
    assert_equal(10, a.max)
    size = 299
    [192, 194, 223, 224, 240, 255, 256].each do |over|
      b = BitVector.new(size)
      b.set(over,1)
      b.set(over+2,1)
      assert_equal(over+2, b.max)
    end
  end

  # BitVector#min
  def test_min
    a = BitVector.new("0")
    assert_equal(nil, a.min)
    a = BitVector.new("1")
    assert_equal(0, a.min)
    a = BitVector.new("11")
    assert_equal(0, a.min)
    a = BitVector.new("0000100001")
    assert_equal(4, a.min)
    size = 299
    [192, 194, 223, 224, 240, 255, 256].each do |over|
      b = BitVector.new(size)
      b.set(over,1)
      b.set(over+2,1)
      assert_equal(over, b.min)
    end
  end

  # BitVector#normalize
  def test_normalize
    a = BitVector.new("0")
    x = a.normalize
    assert_equal("0", x.to_s)
    a = BitVector.new("00000")
    x = a.normalize
    assert_equal("0", x.to_s)
    a = BitVector.new("0101010101010000000")
    x = a.normalize
    assert_equal("010101010101", x.to_s)
    assert_equal("0101010101010000000", a.to_s)
  end

  # BitVector#normalize!
  def test_normalize!
    a = BitVector.new("0")
    x = a.normalize!
    assert_equal("0", a.to_s)
    assert_equal("0", x.to_s)
    a = BitVector.new("00000")
    x = a.normalize!
    assert_equal("0", a.to_s)
    assert_equal("0", x.to_s)
    a = BitVector.new("0101010101010000000")
    x = a.normalize!
    assert_equal("010101010101", x.to_s)
    assert_equal("010101010101", a.to_s)
  end

  def test_index_set
    a = BitVector.new("0000100011")
    assert_equal(4, a.index(1,0))
    assert_equal(4, a.index(1,4))
    assert_equal(8, a.index(1,5))
    size = 42949
    over = 40040
    b = BitVector.new(size)
    b.set(over,1)
    b.set(over+2,1)
    assert_equal(over, b.index(1,0))
    assert_equal(over+2, b.index(1,over+1))
  end

  def test_index_clear
    a = BitVector.new("0000100011")
    assert_equal(0, a.index(0,0))
    assert_equal(5, a.index(0,4))
    assert_equal(nil, a.index(0,8))
    size = 42949
    over = 40040
    b = BitVector.new(size)
    b.set(over,1)
    b.set(over+2,1)
    assert_equal(over, b.index(1,0))
    assert_equal(over+2, b.index(1,over+1))
  end

  def test_count
    a = BitVector.new("0000100011")
    assert_equal(3, a.count)
    size = 42949
    over = 40040
    b = BitVector.new(size)
    b.set(over,1)
    b.set(over+2,1)
    assert_equal(2, b.count)
    b.set(1,1)
    assert_equal(3, b.count)
  end

  # BitVector#to_ary
  def test_to_ary
    a = BitVector.new("00000")
    assert_equal([], a.to_ary)
    a = BitVector.new("01010")
    assert_equal([1,3], a.to_ary)
    a = BitVector.new("1100110101")
    assert_equal([0..1, 4..5, 7, 9], a.to_ary)
    a = BitVector.new("00111111111111011111111111100000000001111111111")
    assert_equal([2..13, 15..26, 37..46], a.to_ary)
  end

  # BitVector#each
  def test_each
    a = BitVector.new("010101011")
    ary = []
    a.each {|idx|
      ary << idx
    }
    assert_equal([1, 3, 5, 7, 8], ary)
  end

end

if $0==__FILE__
  Test::Unit::UI::Console::TestRunner.run(TC_BitVector.suite)
end
