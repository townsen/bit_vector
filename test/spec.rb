# encoding: ASCII-8BIT
#
require 'minitest/autorun'
require 'bit_vector'

describe BitVector do

  describe "new" do

    describe "given a string" do

      it "handles multiples of 8" do
        a = BitVector.new("0000000000000000")
        assert_equal(16, a.size)
        assert_equal(2, a.bytesize)
      end
      it "handles arbitrary sizes" do
        a = BitVector.new("000000000000000")
        assert_equal(15, a.size)
      end
      it "encodes correctly" do
        a = BitVector.new("010000100000001")
        assert_equal(1, a.index(1,0))
        assert_equal(6, a.index(1,2))
        assert_equal(14, a.index(1,7))
      end

    end

    describe "given a length" do

      it "allocates correctly" do
        { 1 => 1, 8 => 1, 9 => 2, 17 => 3, 31 => 4, 33 => 5, 63 => 8, 64 => 8, 65 => 9,
          255 => 32,256 => 32, 65535 => 8192, 65536 => 8192, 4294967295 => 536870912
        }.each do |k,v|
          a = BitVector.new(k)
          assert_equal(v, a.bytesize)
        end

      end
    end
  end

  describe "set and get" do

    it "use [] notation" do
      a = BitVector.new("10101010111111100")
      assert_equal(1,a[0])
      assert_equal(0,a[1])
    end

    it "use []= notation" do
      a = BitVector.new(13)
      a[1] = 1
      assert_equal("0100000000000",a.to_s)
    end

    it "don't check bounds" do
      a = BitVector.new(13)
      a[13] = 1
      assert_equal(0, a[233])
    end

    it "manipulate correctly" do
      [8,16,32,64,128,256,1111].each do |size|
        a = BitVector.new(size)
        [size/2,size-1, size].each do |pos|
          a.set(pos,1)
          assert_equal(1, a.get(pos))
          assert_equal(0, a.get(pos+1))
        end
      end
    end

    it "gets the positions right" do
      a = BitVector.new("1000000001000000110000000010000010100000011000001110000000010000")
      assert_equal(1, a[50])
      assert_equal(0, a[51])
    end
  end

  describe "#from_bytes" do
    it "creates from a string" do
      a = BitVector.from_bytes("1")
      assert_equal("10001100", a.to_s)
    end
    it "creates from a long string" do
      a = BitVector.from_bytes("abcdefghij")
      assert_equal('10000110'+'01000110'+'11000110'+'00100110'+'10100110'+'01100110'+'11100110'+'00010110'+'10010110'+'01010110', a.to_s)
    end
  end

  describe "#bytesize" do
    it "gets the number right" do
      { 1 => 1, 8 => 1, 9 => 2, 17 => 3, 31 => 4, 33 => 5, 63 => 8, 64 => 8, 65 => 9,
        255 => 32,256 => 32, 65535 => 8192, 65536 => 8192
      }.each do |k,v|
        a = BitVector.new(k)
        assert_equal(v, a.bytesize)
      end
    end
  end

  describe "#count" do
    it "counts set bits" do
      a = BitVector.new(33)
      a.set(1,1)
      a.set(6,1)
      a.set(14,1)
      assert_equal(3, a.count)
    end
  end

  describe "#index" do
    before do
      @a = BitVector.new(33)
      @a.set(1,1)
      @a.set(6,1)
      @a.set(14,1)
    end
    it "finds set bits" do
      assert_equal(1, @a.index(1,0))
      assert_equal(6, @a.index(1,2))
      assert_equal(6, @a.index(1,6))
      assert_equal(14, @a.index(1,7))
    end
    it "finds cleared bits" do
      assert_equal(0, @a.index(0,0))
      assert_equal(7, @a.index(0,6))
      assert_equal(13, @a.index(0,13))
    end
    it "returns nil if none set" do
      a = BitVector.new(55)
      assert_equal(nil, a.index(1,0))
    end
  end

  describe "to array" do
    it "handles an empty bit_vector" do
      a = BitVector.new("00000000000000000000000")
      assert_equal [], a.to_ary
    end
    it "handles two single bits" do
      a = BitVector.new("01010")
      assert_equal([1,3], a.to_ary)
    end
    it "handles bit runs" do
      a = BitVector.new("1100110101")
      assert_equal([0..1, 4..5, 7, 9], a.to_ary)
    end
  end

  describe "to_bytes" do
    it "handles arbitrary bytes" do
      a = BitVector.from_bytes("abcdefghijk")
      assert_equal("abcdefghijk", a.to_bytes)
    end
    it "handles short binary" do
      a = BitVector.new("0010000000000001")
      assert_equal("\x04\x80", a.to_bytes)
    end
    it "handles odd bits at end" do
      a = BitVector.new("00100000000000011")
      assert_equal("\x04\x80\x01", a.to_bytes)
    end
    it "handles long binary" do
      a = BitVector.new("1000000001000000110000000010000010100000011000001110000000010000")
      assert_equal("\x01\x02\x03\x04\x05\x06\x07\x08", a.to_bytes)
    end
  end

  describe "resize operator" do
    it "trims it" do
      a = BitVector.new("01010001010101010101000")
      a.size = 7
      assert_equal("0101000", a.to_s)
    end
  end

  describe "normalize operator" do
    it "trims" do
      a = BitVector.new("01010001010101010101000")
      assert_equal("01010001010101010101", a.normalize.to_s)
    end
    it "trims to single bit" do
      a = BitVector.new("0000000000000000")
      b = a.normalize
      assert_equal(1, b.size)
      assert_equal(0, b.get(0))
    end
  end

  describe "min operator" do
    it "finds the bottom" do
      a = BitVector.new("01010001010101010101000")
      assert_equal(1, a.min)
    end
  end

  describe "max operator" do
    it "finds the top" do
      a = BitVector.new("01010001010101010101000")
      assert_equal(19, a.max)
    end
    it "finds the top in a qw" do
      a = BitVector.new("0000000100000010000000110000010000000101000001100000011100001000")
      assert_equal(60, a.max)
    end
    it "finds the top in a +qw" do
      a = BitVector.new("0000000100000010000000110000010000000101000001100000011100001000110100")
      assert_equal(67, a.max)
    end
  end

  describe "not operator" do
    it "works" do
      a = BitVector.new("01010")
      assert_equal("10101", (~a).to_s)
    end
    it "doesn't flip the unused bits" do
      a = BitVector.new("01010")
      b = ~a
      assert_equal(0, b.get(5))
    end
    it "preserves the length when result is empty" do
      a = BitVector.new("1"*35)
      b = ~a
      assert_equal("0"*35, b.to_s)
    end
    it "flips a +qw" do
      a = BitVector.new("0000000100000010000000110000010000000101000001100000011100001000110100")
      b = "1111111011111101111111001111101111111010111110011111100011110111001011"
      assert_equal(b, (~a).to_s)
    end
    it "flips all ones" do
      a = BitVector.new("111111111")
      assert_equal "000000000", (~a).to_s
    end
  end

  describe "and" do
    it "handles the simplest case" do
      a = BitVector.new("1100110101")
      b = BitVector.new("1001001100")
      assert_equal "1000000100", (a & b).to_s
    end
    describe "when the two operands are different sizes" do
      it "treats the missing hi bits as zero" do
        a = BitVector.new("1001")
        b = BitVector.new("1001001100")
        assert_equal "1001", (a & b).to_s
      end
      it "returns a result the size of the shortest" do
        a = BitVector.new("1")
        b = BitVector.new("1001001100")
        assert_equal "1", (a & b).to_s
      end
      it "masks correctly" do
        a = BitVector.new("111111111111111111111111")
        b = BitVector.new("000000000")
        assert_equal "000000000000000000000000", (a & b).to_s
      end
    end
  end

  describe "or" do
    it "handles the simplest case" do
      a = BitVector.new("1100110101")
      b = BitVector.new("1001001100")
      assert_equal "1101111101", (a | b).to_s
    end
    describe "when the two operands are different sizes" do
      it "treats the missing hi bits as zero" do
        a = BitVector.new(1)
        b = BitVector.new("1001001100")
        assert_equal "1001001100", (a | b).to_s
      end
      it "returns a result the size of the longest" do
        a = BitVector.new("1")
        b = BitVector.new("1001001100")
        assert_equal "1001001100", (a | b).to_s
      end
    end
  end

  describe "xor operator" do
    it "ignores unused bits" do
      a = BitVector.new("01010")
      b = BitVector.new("01010")
      c = b ^ a
      assert_equal("00000", c.to_s)
    end
  end

  describe "minus operator" do
    it "subtracts simple" do
      a = BitVector.new("01010")
      b = BitVector.new("01110")
      c = a - b
      assert_equal("00000", c.to_s)
      assert_equal((a & (~b)).to_s, c.to_s)
    end
    it "handles different lengths" do
      a = BitVector.new("111111111111111111111111")
      b = BitVector.new("111111111")
      x = a - b
      assert_equal "000000000111111111111111", x.to_s
    end
  end

  describe "with a set over 2**32 bits" do
    describe "index" do
      it "finds a bit set below 2**32" do
        size = 4_294_967_296
        under32 = 4_004_967_298
        a = BitVector.new(size)
        a.set(under32,1)
        assert_equal under32, a.index(1,0)
      end
    end
    it "finds a bit set above 2**32 bits" do
      size = 6_294_967_296
      over32 = 4_294_967_298
      a = BitVector.new(size)
      a.set(over32,1)
      assert_equal(over32, a.index(1,0))
      assert_equal(over32, a.min)
      assert_equal(over32, a.max)
    end
  end
end
