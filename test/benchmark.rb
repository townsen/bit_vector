#!/usr/bin/env ruby
#
require 'benchmark'
require 'fileutils'
require 'optparse'
require 'ostruct'
require 'bit_vector'
require 'bit_vector/version'

opts = OpenStruct.new( n: 1000000, z: '100k' )

OptionParser.new do |o|
    o.banner = "Benchmark Bitset"

    o.on( '-?', '--help', 'Display this screen' ) do
        puts o
        exit
    end
    o.on('-n', '--number N', Integer, "The number of iterations (default: #{opts.n})") do |n|
	opts.n = n
    end
    o.on('-z', '--size N', String, "The size of each IO (default: #{opts.z})") do |z|
	opts.z = z
    end
    o.parse!
    if /^(?<size>\d+)(?<mult>[kKmM]?)$/ =~ opts.z
        opts.size = size.to_i
        case mult
        when 'k' then opts.size *= 1000
        when 'K' then opts.size *= 1024
        when 'm' then opts.size *= 1000000
        when 'M' then opts.size *= 1054876
        end
    else
	puts "Invalid size specification!"
    end

    if ARGV.size > 0
	puts "You don't need arguments!"
	puts o.help
	exit 2
    end
end

bsa = BitVector.new(opts.size)

puts "Commencing benchmark tests for Bitset #{BitVector::VERSION} \n"
puts "(Testing #{opts.n} iterations with #{opts.size} bits)\n\n"

Benchmark.bm(16) do |x|
    x.report("Random set") do
        opts.n.times do
            bsa.set Random.rand(opts.size), 1
        end
    end
    x.report("Random get") do
        opts.n.times do
            bsa.get Random.rand(opts.size)
        end
    end
    x.report("Random index") do
        opts.n.times do
            off = Random.rand(opts.size)
            bsa.set off, 1
            found = bsa.index 1, 0
        end
    end
end

__END__

  # BitVector#index
  def test_index
    a = BitVector.from_bin("00101010101010101")
    assert_equal(2, a.index(1,0))
    assert_equal(2, a.index(1,2))
    assert_equal(4, a.index(1,3))
  end

  # BitVector#slice
  def test_slice
    a = BitVector.from_bin("10101010101010101")
    b = a.slice(2..7)
    assert_equal("101010", b.to_s)
  end

  # BitVector#clone
  def test_clone
    a = BitVector.from_bin("0")
    b = a.clone
    assert_equal("0", b.to_s)
    
    a = BitVector.from_bin("10101010101010101")
    b = a.clone
    assert_equal("10101010101010101", b.to_s)
  end
  
  # BitVector#get
  def test_get
    a = BitVector.from_bin("1")
    assert_equal(1, a.get(0))
    assert_equal(0, a.get(1))
    assert_equal(0, a.get(100))
    
    a = BitVector.from_bin("10"*50)
    assert_equal(1, a.get(98))
    assert_equal(0, a.get(99))
  end
  
  # BitVector#set
  def test_set
    a = BitVector.new
    a.set 1,1
    assert_equal("01", a.to_s)
    a.set 5,1
    assert_equal("010001", a.to_s)
    a.set 6,1
    assert_equal("0100011", a.to_s)
    a.set 5,0
    assert_equal("0100001", a.to_s)
  end
  
  # BitVector#on
  def test_on
    a = BitVector.new
    a.on 3
    assert_equal("0001", a.to_s)
    a.on 9
    assert_equal("0001000001", a.to_s)
  end
  
  # BitVector#on range
  def test_on_range
    a = BitVector.new
    a.on 1..3
    assert_equal("0111", a.to_s)
    a.on 6..10
    assert_equal("01110011111", a.to_s)
  end
  
  # BitVector#off
  def test_off
    a = BitVector.from_bin("11111111111111111111")
    a.off 5
    assert_equal("11111011111111111111", a.to_s)
    a.off 9
    assert_equal("11111011101111111111", a.to_s)
  end
  
  # BitVector#off
  def test_off_range
    a = BitVector.from_bin("11111111111111111111")
    a.off 2..4
    assert_equal("11000111111111111111", a.to_s)
    a.off 6..10
    assert_equal("11000100000111111111", a.to_s)
  end
  
  # BitVector#clear
  def test_clear
    a = BitVector.from_bin("0")
    a.clear
    assert_equal(1, a.size)
    assert_equal("0", a.to_s)
    
    a = BitVector.from_bin("1111")
    a.clear
    assert_equal(4, a.size)
    assert_equal("0000", a.to_s)
    
    a = BitVector.from_bin("111100001111000011")
    a.clear
    assert_equal(18, a.size)
    assert_equal("000000000000000000", a.to_s)
  end

  # BitVector#|
  def test_or
    a = BitVector.from_bin("000000")
    b = BitVector.from_bin("111000")
    x = a | b
    assert_equal("000000", a.to_s)
    assert_equal("111000", b.to_s)
    assert_equal("111000", x.to_s)
    
    a = BitVector.from_bin("0101010101")
    b = BitVector.from_bin("1111100000")
    x = a | b
    assert_equal("0101010101", a.to_s)
    assert_equal("1111100000", b.to_s)
    assert_equal("1111110101", x.to_s)
    
    a = BitVector.from_bin("00000000000001")
    b = BitVector.from_bin("00000000000010")
    x = a | b
    assert_equal("00000000000011", x.to_s)
    
    a = BitVector.from_bin("1")
    b = BitVector.from_bin("000000001")
    x = a | b
    assert_equal("100000001", x.to_s)

    a = BitVector.from_bin("000000001")
    b = BitVector.from_bin("1")
    x = a | b
    assert_equal("100000001", x.to_s)
  end
  
  # BitVector#+
  def test_plus
    a = BitVector.from_bin("000000")
    b = BitVector.from_bin("111000")
    x = a + b
    assert_equal("000000", a.to_s)
    assert_equal("111000", b.to_s)
    assert_equal("111000", x.to_s)
    
    a = BitVector.from_bin("0101010101")
    b = BitVector.from_bin("1111100000")
    x = a + b
    assert_equal("0101010101", a.to_s)
    assert_equal("1111100000", b.to_s)
    assert_equal("1111110101", x.to_s)
    
    a = BitVector.from_bin("00000000000001")
    b = BitVector.from_bin("00000000000010")
    x = a + b
    assert_equal("00000000000011", x.to_s)
    
    a = BitVector.from_bin("1")
    b = BitVector.from_bin("000000001")
    x = a + b
    assert_equal("100000001", x.to_s)

    a = BitVector.from_bin("000000001")
    b = BitVector.from_bin("1")
    x = a + b
    assert_equal("100000001", x.to_s)
  end

  # BitVector#-
  def test_minus
    a = BitVector.from_bin("01010101")
    b = BitVector.from_bin("00001111")
    x = a - b
    assert_equal("01010101", a.to_s)
    assert_equal("00001111", b.to_s)
    assert_equal("01010000", x.to_s)
    
    a = BitVector.from_bin("111111111111111111111111")
    b = BitVector.from_bin("111111111")
    x = a - b
    assert_equal("000000000111111111111111", x.to_s)
    x = b - a
    assert_equal("000000000", x.to_s)
  end

  # BitVector#<=>
  def test_cmp
    a = BitVector.from_bin("0")
    b = BitVector.from_bin("1")
    assert_equal(-1, a <=> b)
    assert_equal(0, a <=> a)
    assert_equal(1, b <=> a)
    
    a = BitVector.from_bin("1000000000")
    b = BitVector.from_bin("1")
    assert_equal(0, a <=> b)
  end

  # BitVector#&
  def test_and
    a = BitVector.from_bin("010111")
    b = BitVector.from_bin("001110")
    x = a & b
    assert_equal("010111", a.to_s)
    assert_equal("001110", b.to_s)
    assert_equal("000110", x.to_s)

    a = BitVector.from_bin("01010101")
    b = BitVector.from_bin("00001111")
    x = a & b
    assert_equal("01010101", a.to_s)
    assert_equal("00001111", b.to_s)
    assert_equal("00000101", x.to_s)

    a = BitVector.from_bin("0")
    b = BitVector.from_bin("1111111111")
    x = a & b
    assert_equal("0", x.to_s)
    x = b & a
    assert_equal("0000000000", x.to_s)
    
    a = BitVector.from_bin("11111111")
    b = BitVector.from_bin("1111111111111111")
    x = a & b
    assert_equal("11111111", x.to_s)
    x = b & a
    assert_equal("1111111100000000", x.to_s)
    
    a = BitVector.from_bin("11001100")
    b = BitVector.from_bin("1110111011101110")
    x = a & b
    assert_equal("11001100", x.to_s)
    x = b & a
    assert_equal("1100110000000000", x.to_s)
  end

  # BitVector#^
  def test_xor
    a = BitVector.from_bin("01010101")
    b = BitVector.from_bin("00110011")
    x = a ^ b
    assert_equal("01010101", a.to_s)
    assert_equal("00110011", b.to_s)
    assert_equal("01100110", x.to_s)
    
    a = BitVector.from_bin("0")
    b = BitVector.from_bin("10101010101010")
    x = a ^ b
    assert_equal("10101010101010", x.to_s)
    x = b ^ a
    assert_equal("10101010101010", x.to_s)
  end

  # BitVector#*
  def test_multi
    a = BitVector.from_bin("010111")
    b = BitVector.from_bin("001110")
    x = a * b
    assert_equal("010111", a.to_s)
    assert_equal("001110", b.to_s)
    assert_equal("000110", x.to_s)

    a = BitVector.from_bin("01010101")
    b = BitVector.from_bin("00001111")
    x = a * b
    assert_equal("01010101", a.to_s)
    assert_equal("00001111", b.to_s)
    assert_equal("00000101", x.to_s)

    a = BitVector.from_bin("0")
    b = BitVector.from_bin("1111111111")
    x = a * b
    assert_equal("0", x.to_s)
    x = b * a
    assert_equal("0000000000", x.to_s)
    
    a = BitVector.from_bin("11111111")
    b = BitVector.from_bin("1111111111111111")
    x = a * b
    assert_equal("11111111", x.to_s)
    x = b * a
    assert_equal("1111111100000000", x.to_s)
    
    a = BitVector.from_bin("11001100")
    b = BitVector.from_bin("1110111011101110")
    x = a * b
    assert_equal("11001100", x.to_s)
    x = b * a
    assert_equal("1100110000000000", x.to_s)
  end
  
  # BitVector#~
  def test_not
    a = BitVector.from_bin("0")
    x = ~a
    assert_equal("0", a.to_s)
    assert_equal("1", x.to_s)
    
    a = BitVector.from_bin("0101")
    x = ~a
    assert_equal("1010", x.to_s)
    
    a = BitVector.from_bin("00000000001111111111")
    x = ~a
    assert_equal("11111111110000000000", x.to_s)
  end

  # BitVector#zero?
  def test_zero?
    a = BitVector.from_bin("0")
    assert_equal(true, a.zero?)
    a = BitVector.from_bin("0"*100)
    assert_equal(true, a.zero?)
    
    a = BitVector.from_bin("1")
    assert_equal(false, a.zero?)
    a = BitVector.from_bin("0"*100 + "1")
    assert_equal(false, a.zero?)
    
  end
  
  # BitVector#nonzero?
  def test_nonzero?
    a = BitVector.from_bin("0")
    assert_equal(false, a.nonzero?)
    a = BitVector.from_bin("0"*100)
    assert_equal(false, a.nonzero?)
    
    a = BitVector.from_bin("1")
    assert_equal(true, a.nonzero?)
    a = BitVector.from_bin("0"*100 + "1")
    assert_equal(true, a.nonzero?)
  end
  
  # BitVector#max
  def test_max
    a = BitVector.from_bin("0")
    assert_equal(nil, a.max)
    a = BitVector.from_bin("1")
    assert_equal(0, a.max)
    a = BitVector.from_bin("11")
    assert_equal(1, a.max)
    a = BitVector.from_bin("10000000001")
    assert_equal(10, a.max)
  end
  
  # BitVector#min
  def test_min
    a = BitVector.from_bin("0")
    assert_equal(nil, a.min)
    a = BitVector.from_bin("1")
    assert_equal(0, a.min)
    a = BitVector.from_bin("11")
    assert_equal(0, a.min)
    a = BitVector.from_bin("0000100001")
    assert_equal(4, a.min)
  end

  # BitVector#normalize
  def test_normalize
    a = BitVector.from_bin("0")
    x = a.normalize
    assert_equal("0", x.to_s)
    a = BitVector.from_bin("00000")
    x = a.normalize
    assert_equal("0", x.to_s)
    a = BitVector.from_bin("0101010101010000000")
    x = a.normalize
    assert_equal("010101010101", x.to_s)
    assert_equal("0101010101010000000", a.to_s)
  end
  
  # BitVector#normalize!
  def test_normalize!
    a = BitVector.from_bin("0")
    x = a.normalize!
    assert_equal("0", a.to_s)
    assert_equal("0", x.to_s)
    a = BitVector.from_bin("00000")
    x = a.normalize!
    assert_equal("0", a.to_s)
    assert_equal("0", x.to_s)
    a = BitVector.from_bin("0101010101010000000")
    x = a.normalize!
    assert_equal("010101010101", x.to_s)
    assert_equal("010101010101", a.to_s)
  end
  
  # BitVector#to_ary
  def test_to_ary
    a = BitVector.from_bin("00000")
    assert_equal([], a.to_ary)
    a = BitVector.from_bin("01010")
    assert_equal([1,3], a.to_ary)
    a = BitVector.from_bin("1100110101")
    assert_equal([0..1, 4..5, 7, 9], a.to_ary)
    a = BitVector.from_bin("00111111111111011111111111100000000001111111111")
    assert_equal([2..13, 15..26, 37..46], a.to_ary)
  end
  
  # BitVector#each
  def test_each
    a = BitVector.from_bin("010101011")
    ary = []
    a.each {|idx|
      ary << idx
    }
    assert_equal([1, 3, 5, 7, 8], ary)
  end

  # BitVector#to_bytes
  def test_to_bytes
    a = BitVector.new("abc")
    assert_equal("abc", a.to_bytes)
    a = BitVector.from_bin("00000000000000000")
    assert_equal("\x00\x00\x00", a.to_bytes)
  end
  
end


if $0==__FILE__
  Test::Unit::UI::Console::TestRunner.run(TC_BitVector.suite)
end
