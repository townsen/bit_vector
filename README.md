# bit_vector

An implementation of arbitrarily large Bit Vectors in Ruby using C + inline assembler for speed

This builds heavily on a much earlier Gem 'bitset' authored by 'MoonWolf' on RAA.

## Known Bugs

* As of release ~> 0.9 it _only_ works on MacOS X Yosemite. It segfaults on Linux!
* The `index` method does not work on cleared bits, for set bits it does

## Notes on Bit Vector Lengths

Many bit vector operations are only defined if the lengths of the vectors are the same. By
considering a bit vector to possess any number of high-order zeros most operations
can be made to work with differently sized operands. When creating this library
a couple of different approaches to this were considered:

* Return a result the size of the first operand,
* Return the smallest result set (not necessarily normalized),
* Return a normalized result (the length being the position of the high order bit).

Like the original implementor I chose to use the middle strategy.

## Bit Vectors in String Format

The string format of a bit vector represents the bit positions from least significant to
most significant with the characters `0`,`-`,`f`,`F`,`N` representing a clear bit (zero) and all other
characters interpreted as a set bit (one).

Bit indices are zero-relative

## Usage

Quickstart:
```ruby
a = BitVector.new(13) # 13 bits long, all zero
a[3] = 1
a[4] = 2
puts a.to_s # '00011000000000'
puts "The higher bits are all #{a[1930]} (zero)"

b = BitVector.new("00001011011")
puts "The first bit is #{b[0]}" # 0
puts "The eleventh bit is #{b[10]}" # 1
puts "The first set bit is #{b.index(0,1)}" # 4
puts "The first clear bit after the 5th is #{b.index(1,5)}" # 6
```
## `from_bytes` and `to_bytes`

These are methods to serialize and de-serialize a bit vector in a platform dependent way.
They use ASCII-8BIT encoded strings that represents the bit vector.
It is _not_ the binary representation as a string of 0's and 1's used by `new` and `to_s` methods.

## Operators and Methods

Statements that use binary operators of the form `r = a op b` are computed by
the Ruby call `a.op(b)`

Most operators/methods are obvious, here's some that aren't

* _minus_ - defined elementwise as: `r = a and not b`
* `bytesize` - the length in bytes required to store this bitvec

